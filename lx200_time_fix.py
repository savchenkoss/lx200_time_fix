#!/usr/bin/env python

import time
import datetime
from serial import Serial
from serial import SerialException


class Telescope(object):
    def __init__(self):
        while True:
            print("Connecting to the telescope...")
            try:
                self.device = Serial("/dev/ttyS0", baudrate=9600, timeout=1)
                break
            except SerialException:
                print("Connection failed.")
                print("Hints: Is the telescope turned on? The Sky6 should be closed!")
                print("Waiting 5 sec before trying again.")
                time.sleep(5)
                print("\n\n\n")
        print("Connection established.")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def send_command(self, command, response_lines=1):
        self.device.flushOutput()
        self.device.write(str.encode(command))
        response = self.get_response(response_lines)
        return response

    def get_response(self, lines_desired=1):
        """Read output until end_str is found"""
        response = []
        while len(response) < lines_desired:
            new_line = self.device.readline().decode("utf-8")
            response.append(new_line)
            time.sleep(0.05)
        return "".join(response)

    def set_initial_date_and_time(self):
        """
        :hIYYMMDDHHMMSS#
        Bypass handbox entry of daylight savings, date and time. Use the values supplied in
        this command. This feature isintended to allow use of the Autostar II from permanent
        installations where GPS reception is not possible, such as withinmetal domes.  This
        command must be issued while the telescope is waiting at the initial daylight savings
        prompt.Returns: 1 – if command was accepted.
        """
        localtime = datetime.datetime.now()
        hour = localtime.hour + 3  # Convert UTC that is set on the telescope PC to localtime
        minute = localtime.minute
        second = localtime.second
        day = localtime.day
        month = localtime.month
        year = localtime.year
        command = ":hI"
        command += "%s%02i%02i" % (str(year)[-2:], month, day)
        command += "%02i%02i%02i#" % (hour, minute, second)
        response = self.send_command(command, response_lines=1)
        print(response)

    def set_local_time(self):
        """Set local time using the LX200 protocol comand :SLHH:MM:SS# """
        localtime = datetime.datetime.now()
        hour = localtime.hour + 3  # Convert UTC that is set on the telescope PC to localtime
        minute = localtime.minute
        second = localtime.second
        command = ":SL%02i:%02i:%02i#" % (hour, minute, second)
        self.send_command(command, response_lines=1)

    def set_current_date(self):
        """Set current date using LX200 protocol command :SCMM/DD/YY#"""
        localtime = datetime.datetime.now()
        day = localtime.day
        month = localtime.month
        year = localtime.year
        command = ":SC%02i/%02i/%s#" % (month, day, str(year)[-2:])
        self.send_command(command, response_lines=2)

    def get_local_time(self):
        command = ":GL#"
        response = self.send_command(command, response_lines=1)
        return response

    def get_current_date(self):
        command = ":GC#"
        response = self.send_command(command, response_lines=1)
        return response

    def close(self):
        print("Closing connection.")
        self.device.close()
        print("Connection closed.")


welcome_str = r"""
 __    _  _     ___   ___    ___
(  )  ( \/ )___(__ \ / _ \  / _ \
 )(__  )  ((___)/ _/( (_) )( (_) )
(____)(_/\_)   (____)\___/  \___/
"""


def main():
    print(welcome_str)
    with Telescope() as tel:
        print("\nSetting current date and time...")
        # tel.set_local_time()
        # tel.set_current_date()
        tel.set_initial_date_and_time()
        print("Success!")

        print("\nCurrent date and time")
        print("  %s" % tel.get_current_date()[:-1])
        print("  %s\n" % tel.get_local_time()[:-1])
    input("\nAll Done. Press Enter to exit. ")


if __name__ == '__main__':
    main()
